.PHONY: build push run

all: run

build: build.sh Dockerfile killme.sh task01.sh
	docker build --rm --no-cache -t jirib/showmax-interview-rogue-process:latest .

push: build
	docker push jirib/showmax-interview-rogue-process:latest

run:
	docker pull jirib/showmax-interview-rogue-process:latest
	docker run --rm -it jirib/showmax-interview-rogue-process:latest
