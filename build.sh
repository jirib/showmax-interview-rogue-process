#!/bin/sh -eu
apk update
apk add bash

rm -f /bin/egrep
rm -f /bin/grep
rm -f /bin/ps
rm -f /usr/bin/find
rm -f /usr/bin/pgrep
rm -f /usr/bin/pkill
rm -f /usr/bin/pstree
rm -f /usr/bin/top
rm -f /usr/bin/xargs
rm -f /sbin/apk
rm -f "$0"

echo 'alias kill="kill -SIGQUIT"' > /etc/profile.d/kill.sh
