FROM alpine:latest
COPY . /task01/
RUN /task01/build.sh
ENTRYPOINT /task01/task01.sh
