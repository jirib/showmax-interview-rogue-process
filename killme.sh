#!/bin/sh -eu

TMPDIR="/usr/sbin/psutils"
FILENAME="${TMPDIR}/killme.${RANDOM}.sh"
PROGRAM="$(cat "$0")"
rm -f "$0"

R=$(( $RANDOM % 100 ));
if [ "${R}" = 0 ]; then
  echo "I am a rogue process. Your goal is to kill me. Can you do that?"
fi
sleep 0.1

mkdir -p "${TMPDIR}"
chown root:root "${TMPDIR}"
echo "${PROGRAM}" > "${FILENAME}"
chmod +x "${FILENAME}"

"${FILENAME}" &
